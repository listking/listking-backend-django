from rest_framework import viewsets
from rest_framework import permissions

from api.models import Item, ItemList
from django.contrib.auth.models import User, Group
from api.permissions import IsOwner

from api.serializers import UserSerializer, GroupSerializer
from api.serializers import ItemSerializer, ItemListSerializer


class UserViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """

    queryset = User.objects.all().order_by("-date_joined")
    serializer_class = UserSerializer
    permission_classes = [permissions.IsAuthenticated]


class GroupViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows groups to be viewed or edited.
    """

    queryset = Group.objects.all()
    serializer_class = GroupSerializer
    permission_classes = [permissions.IsAuthenticated]


class ItemListViewSet(viewsets.ModelViewSet):
    serializer_class = ItemListSerializer
    permission_classes = [permissions.IsAuthenticatedOrReadOnly, IsOwner]

    # queryset = ItemList.objects.all()
    def get_queryset(self):
        user = self.request.user
        return ItemList.objects.filter(owner=user)

    def perform_create(self, serializer):
        serializer.save(owner=self.request.user)


class ItemViewSet(viewsets.ModelViewSet):
    serializer_class = ItemSerializer
    permission_classes = [permissions.IsAuthenticatedOrReadOnly, IsOwner]

    # queryset = Item.objects.filter(owner=self.request.user)
    def get_queryset(self):
        user = self.request.user
        return Item.objects.filter(owner=user)

    def perform_create(self, serializer):
        serializer.save(owner=self.request.user)
