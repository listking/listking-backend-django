from django.db import models


class ItemList(models.Model):
    list_name = models.CharField(max_length=100)
    owner = models.ForeignKey(
        "auth.User", related_name="lists", on_delete=models.CASCADE
    )

    def __str__(self):
        return f"{self.list_name}"


class Item(models.Model):
    list_name = models.ForeignKey(
        ItemList, related_name="items", on_delete=models.CASCADE
    )
    created = models.DateTimeField(auto_now_add=True)
    title = models.CharField(max_length=100, blank=True, default="")
    isComplete = models.BooleanField(default=False)
    owner = models.ForeignKey(
        "auth.User", related_name="items", on_delete=models.CASCADE
    )

    class Meta:
        ordering = ["created"]

    def __str__(self):
        return f"{self.title}"
