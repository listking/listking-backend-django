from django.contrib.auth.models import User, Group
from rest_framework import serializers

from api.models import Item, ItemList


class UserSerializer(serializers.HyperlinkedModelSerializer):
    lists = serializers.PrimaryKeyRelatedField(
        many=True, queryset=ItemList.objects.all()
    )

    class Meta:
        model = User
        fields = ["url", "username", "email", "groups", "lists"]


class GroupSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Group
        fields = ["url", "name"]


class ItemListSerializer(serializers.ModelSerializer):
    items = serializers.StringRelatedField(many=True)
    owner = serializers.ReadOnlyField(source='owner.username')

    class Meta:
        model = ItemList
        fields = ["id", "list_name", "items", "owner"]


class ItemSerializer(serializers.ModelSerializer):
    owner = serializers.ReadOnlyField(source='owner.username')

    class Meta:
        model = Item
        fields = ["id", "title", "isComplete", "owner", "list_name"]
