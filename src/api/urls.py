from django.urls import include, path
from api import views
from rest_framework import routers


router = routers.DefaultRouter()
router.register(r'item-lists', views.ItemListViewSet, basename='item-lists')
router.register(r'items', views.ItemViewSet, basename='items')

urlpatterns = [
    path('', include(router.urls)),
]
